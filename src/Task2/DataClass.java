package Task2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DataClass {
    Integer[] intArr = new Integer[25];

    public Integer[] fillArrayWithRandNumb(Integer[] intArr){
        for (int i = 0; i<intArr.length; i++){
            intArr[i] = (int) ((Math.random()*(200+1)) - 100);
        }


        return intArr;
    }

    public Integer[] returnSquareArrayNumbersInOrderSort(Integer[] intArr){
        Integer[] resultArr = new Integer[intArr.length];
        for (int i = 0; i<intArr.length; i++){
            resultArr[i] = (int) Math.pow(intArr[i],2);
        }
        System.out.println("\n\nRandom numbers in square Array:");
        showUpArray(resultArr);
        Arrays.sort(resultArr);

        return resultArr;
    }

    public Integer[] showUpArray(Integer[] intArr){
        for (int i = 0; i<intArr.length; i++){
            System.out.println("element " + intArr[i]);
        }

        return intArr;
    }
}
