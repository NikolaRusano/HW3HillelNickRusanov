package Task2;

public class DemoDataClass {
    public static void main(String[] args) {
        DataClass dataClass = new DataClass();
        Integer[] randArr = dataClass.fillArrayWithRandNumb(dataClass.intArr);
        System.out.println("Random Array:");
        dataClass.showUpArray(randArr);
        Integer[] sortedSquareArray = dataClass.returnSquareArrayNumbersInOrderSort(randArr);
        System.out.println("Square Array Numbers In Order Sorted:");
        dataClass.showUpArray(sortedSquareArray);

    }
}
