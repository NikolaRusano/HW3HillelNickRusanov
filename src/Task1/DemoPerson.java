package Task1;

import org.w3c.dom.ls.LSOutput;

public class DemoPerson {
    public static void main(String[] args) {


        String firstName = "Chandler";
        String lastName = "Bing";
        String middleName = "Muriel";

        Person person = new Person(lastName, firstName, middleName);
        System.out.println(person.toString());
        String fullName = "Остап-Сулейман Берта-Мария Бендер-Задунайский";
        Person person2 = new Person(fullName);
        System.out.println(person2.toString());

    }
}
