package Task1;

public class Person {
    String lastName, firstName, middleName;
    String fullName;

    public Person(String lastName, String firstName, String middleName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
    }

    public Person(String fullName) {
        this.fullName = fullName;
        String []nameWordsArr = this.fullName.split(" ");
        this.lastName = nameWordsArr[0];
        this.firstName = nameWordsArr[1];
        this.middleName = nameWordsArr[2];
    }

    @Override
    public String toString() {
        return "Person{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                '}';
    }

}
