package Task3;

import java.util.ArrayList;
import java.util.List;

public class StringVariations {
    String strForCheck;

    public long factorialNumb(int n){
        long fact = 1;
        for(int i=0; i<=n; i++){
            fact = fact*i;
        }
        return fact;
    }

    public boolean isStringInsideArr(List<String> strArr, String isStrInside){
        for (String item: strArr){
            if (item.equals(isStrInside)){
                return true;
            }
        }
        return false;
    }

    public void showArray(char []Arr){
        for (char item: Arr){
            System.out.println(item);
        }
    }

    public List<String> stringMixVariations(String strForCheck){
        StringBuilder []mixVariations = new StringBuilder[(int) factorialNumb(strForCheck.length())];
        int pnc = 0;
        char []charArr = strForCheck.toCharArray();
        List<String> listmixVariations = new ArrayList<>();
        showArray(charArr);
        for(int i=0;i<charArr.length;i++){
            for(int j=0;j<charArr.length;j++){
                for(int k=0;k<charArr.length;k++){
                    if(i!=j && i!=k && j!=k){
                        char []tempArr = {charArr[i],charArr[j],charArr[k]};
                        String tempStr = tempArr.toString();
                        if (!isStringInsideArr(listmixVariations,tempStr))
                            listmixVariations.add(tempStr.toString());
                            pnc++;

                    }
                }
            }
        }
        return listmixVariations;
    }
}
