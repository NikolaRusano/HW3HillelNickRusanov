package Task3;

import java.util.List;

public class DemoStringVariants {
    public static void main(String[] args) {
        String stringForTest = "abc";
        StringVariations stringVariations = new StringVariations();
        List<String> resStringBuilder = stringVariations.stringMixVariations(stringForTest);
        for(String item :resStringBuilder){
            System.out.println(item.toString());
        }
    }
}
